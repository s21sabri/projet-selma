using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CreateOnClick : MonoBehaviour
{
    public GameObject aCopier;
    public InputAction CreerObject;
    public int force = 1000;
 
    
    void Start()
    {
        CreerObject.Enable();
    }

    void Update() {
        // Vérifiez si la touche "P" est enfoncée
        if (Input.GetKeyDown(KeyCode.P)) {
            // Calculez la direction de la caméra
            Vector3 cameraDirection = Camera.main.transform.forward;

            // Créez une instance du cube jaune prefab en ajoutant un offset pour qu'il ne soit pas à l'intérieur de la caméra
            Vector3 position = transform.position + cameraDirection * 2f;
            GameObject newObj = Instantiate(aCopier, position, Quaternion.identity);
            newObj.GetComponent<Renderer>().material.color = Color.yellow;
            newObj.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * force);
        }
    }
}   